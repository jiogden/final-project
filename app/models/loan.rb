class Loan < ActiveRecord::Base
  def self.payment
    @@payment
  end

  def self.amort_table(price = 0.00, interest_rate = 0.00, period = 0)
    Rails.logger.debug "Inputs: #{price} #{interest_rate} #{period}"
    loan = []
    fixed = price
    (1..period*12).each do |counter|
      period_rate = interest_rate/12
      @@payment = ((period_rate*fixed)/(1-(1+period_rate)**(-period*12)))
      loan << price - (@@payment - (price*interest_rate))

      last = price - (@@payment - (price*period_rate))

      price = last
    end
    return loan
  end

  def amort_table
    return self.class.amort_table(self.price, self.interest_rate, self.period)
  end
end
