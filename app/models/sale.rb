class Sale < ActiveRecord::Base
  belongs_to :car
  belongs_to :customer
  belongs_to :staff

  @@markval=0.10
  @@tax=0.043

  def sold
    if self.status == false
      return "Not Sold"
    else
      return "Sold"
    end
  end

  def calc
    markups = (msrp + (msrp * @@markval))
    return markups
  end

  def taxx
    tax_total = (calc_total * @@tax)
    return tax_total
  end

  def marks
    mark = (msrp * 0.10)
    return mark
  end

  def calc_total
    final = ((calc + (calc * @@tax) + (calc * interest)) - principal)
    return final
  end

  def self.grevenue
    gross=0
    Sale.all.each do |counter|
      if counter.status == true
        gross += counter.calc_total
      end
    end
    return gross
  end

  def self.netprofit
    net=0
    Sale.all.each do |money|
      if money.status == true
        net += money.marks
      end
    end
    return net
  end

  def self.taxtotal
    tax=0
    Sale.all.each do |taxes|
      if taxes.status == true
        tax += taxes.taxx
      end
    end
    return tax
  end


end

