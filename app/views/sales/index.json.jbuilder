json.array!(@sales) do |sale|
  json.extract! sale, :id, :msrp, :markup, :interest, :principal, :total, :status, :customer_id, :staff_id, :car_id
  json.url sale_url(sale, format: :json)
end
