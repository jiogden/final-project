json.array!(@loans) do |loan|
  json.extract! loan, :id, :price, :interest_rate, :period
  json.url loan_url(loan, format: :json)
end
