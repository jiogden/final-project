# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

build_table = ->
  price = $("#loan_price").val()
  interest_rate = $("#loan_interest_rate").val()
  period =  $("#loan_period").val()
  $.get "/loans/loan",
    {price: price, interest_rate: interest_rate, period: period }

$ ->
  $('#loan_price, #loan_interest_rate, #loan_period').bind 'keyup mouseup mousewheel', ->
    build_table()

  build_table()

