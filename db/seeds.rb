# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Car.delete_all
Customer.delete_all
Staff.delete_all
Sale.delete_all

Car.create(make: "Ford", model:"Mustang", vin:"1FMJU1F50AEA43173", color: "Blue")
Car.create(make:"Ford", model: "Escape", vin:"1FMJU1F50AEB03730", color: "Red")
Car.create(make:"Ford", model:"Edge", vin:"1FMJU1F50AEA53881",color: "Green")
Car.create(make:"Ford", model:"Explorer", vin:"1FMJU1F50AEB47629",color: "Black")
Car.create(make:"Ford", model:"Taurus", vin:"1FMJU1F50AEA81194",color: "Orange")
Car.create(make:"Ford", model:"Ranger", vin:"1FMJU1F50AEB49008",color: "Yellow")
Car.create(make:"Ford", model:"Focus",vin:"1FMJU1F50AEA93927",color: "Pink")
Car.create(make:"Ford",  model:"F-150",vin:"1FMJU1F50AEB53138",color: "Rainbow")
Car.create(make:"Ford", model:"Ecoline", vin:"1FMJU1F50AEB72319",color: "Magenta")
Car.create(make:"Ford", model:"Expedition", vin:"1FMJU1F51AEB48854",color: "Grey")


Customer.create(name:"Josh")
Customer.create(name:"Ben")
Customer.create(name:"Jake")
Customer.create(name:"Santa")
Customer.create(name:"Charles")


Staff.create(name:"Charles")
Staff.create(name:"Timmy")

Sale.create(msrp: 10000, markup: 11000, interest: 0.2 , principal: 2000 , total: 9616.46 , status: "1" , customer_id: "1" , staff_id: "2" , car_id: "4")
Sale.create(msrp: 5000, markup: 5500, interest: 0.1 , principal: 1000 , total: 4750.87  , status: "0" , customer_id: "2" , staff_id: "2" , car_id: "3")
Sale.create(msrp: 8000, markup: 8800, interest: 0.3 , principal: 1500 , total: 7889.25 , status: "1" , customer_id: "4" , staff_id: "1" , car_id: "5")