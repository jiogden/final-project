class CreateLoans < ActiveRecord::Migration
  def change
    create_table :loans do |t|
      t.decimal :price
      t.decimal :interest_rate
      t.integer :period

      t.timestamps null: false
    end
  end
end
