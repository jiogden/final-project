class CreateSales < ActiveRecord::Migration
  def change
    create_table :sales do |t|
      t.decimal :msrp
      t.decimal :markup
      t.decimal :interest
      t.decimal :principal
      t.decimal :total
      t.boolean :status
      t.integer :customer_id
      t.integer :staff_id
      t.integer :car_id

      t.timestamps null: false
    end
  end
end
